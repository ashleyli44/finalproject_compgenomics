#runs our main ML algorithms
#generates our figures
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from sklearn.mixture import GaussianMixture
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
import pandas as pd
import argparse
from sklearn.manifold import TSNE
'''
    Writes a phenotype file .CLS for cluster means
    @param fname is the filename to write to
    @param clusters_mat is the array of cluster means
'''
def writeCLS(fname, cluster_mat):
    f = open(fname, 'w')
    f.write('#numeric\n')
    for i in range(0, cluster_mat.shape[0]):
        f.write("#Label_%d\n" %i)
        for j in range(0, cluster_mat.shape[1]):
            f.write("%f " %cluster_mat[i,j])
        f.write("\n")

'''
    Generates a data frame
    @param labels is a n x 1, labels for all samples
    @param data is the nsamples x nfeatures matrix
    @param k is the number of clusters
'''
def genDF(labels, data, k):
    #Finding the averages of the data points w/ label i
    avg = np.zeros((k, data.shape[1]))
    for i in range(0, k):
        avg[i,:] = np.mean(data[labels == i, :], axis = 0)
    #adding labels to cluster means
    arr = np.zeros((avg.shape[0], avg.shape[1]+1))
    for i in range(avg.shape[0]):
        arr[i,0:avg.shape[1]] = avg[i,:]
        arr[i,avg.shape[1]] = i
    df = pd.DataFrame(dict(tweleve = arr[:,0], twofour = arr[:,1], foureight =  arr[:,2], seventwo =  arr[:,3], label = arr[:,4]))
    return df

'''
    Function calculates the BIC score
    @param data is the feature matrix (nsamples vs nfeatures)
    @param kmeans is the results of the kmeans obk
    @param num_clusters it the total number of clusters
'''
def BIC(data, kmeans, num_clusters):
    return kmeans.inertia_ + num_clusters*data.shape[1] * np.log(data.shape[0])

'''
    Function generates the bic scores for kmeans and GMM
    @param pcaFeatures is the pca features
    @param kRange is the range of kvalues
    @return bicscores of kmeans and bicscoresGM
'''
def bic_plot(data, kRange):
    bicScores = np.zeros((len(kRange),))
    bicScoresGM = np.zeros((len(kRange),))
    for i in kRange:
        kmeansResult = KMeans(n_clusters = i).fit(data)
        gm = GaussianMixture(n_components = i).fit(data)
        bicScores[i-2] = BIC(data, kmeansResult, i)
        bicScoresGM[i-2] = gm.bic(data)
    return bicScores, bicScoresGM

'''
    Generates bic plots from given krange and bicscores
    @param kRange is the range of k clusters
    @param bicScore_km bic scores from kmeans
    @param bicScore_gmm bic scores from gmm
    @param cell is the cell type
    @param result_dir is the directory to save the figures
'''
def gen_bic_plot(kRange, bicScore_km, bicScore_gmm, cell, result_dir):
    km_ofile = result_dir+"Bicplot_KM_"+cell+".png"
    gmm_ofile = result_dir+"Bicplot_GMM_"+cell+".png"
    
    km_title = "BIC Score vs Number of Clusters for KMeans "+ cell
    gmm_title = "BIC Score vs Number of Clusters for GMM " + cell 
    plt.figure()
    plt.plot(kRange, bicScore_km)
    plt.xlabel("Number of Clusters")
    plt.ylabel("BIC Score")
    plt.title(km_title)
    plt.savefig(km_ofile)
    plt.clf()

    plt.figure()
    plt.plot(kRange, bicScore_gmm)
    plt.xlabel("Number of Clusters")
    plt.ylabel("BIC Score")
    plt.title(gmm_title)
    plt.savefig(gmm_ofile)
    plt.clf()

'''
    Generates cluster figures
    @param features is the data matrix
    @param label_vect is vector of labels
    @param cluster_type kmeans, gmm, or dbscan
    @param cell is the cell type
    @param results_dir is the results directory to put the results in
'''
def gen_cluster_plot(feature, label_vect, cluster_type, cell, results_dir):
    df = pd.DataFrame(dict(x = feature[:,0], y = feature[:,1], label = label_vect))
    groups = df.groupby('label')
    fig, ax = plt.subplots()
    for name, group in groups:
        ax.scatter(group.x, group.y, label = name)
    ax.legend(loc = 'upper right')
    title = cluster_type + " clusters " + cell
    ofile = results_dir + "cluster_"+cluster_type + cell + ".png"
    plt.title(title)
    plt.xlabel("TSNE 1")
    plt.ylabel("TSNE 2")
    plt.savefig(ofile)
    plt.clf()

'''
    Generates the mean cluster plots
    @param time are the time points
    @param clusterDF is the dataframe of cluster means
    @param k is the number of clusters
    @param cell is the cell type
    @param ctype is the clustering type, kmeans, gmm, dbscan
    @param results_dir is the dir to put the results in
'''
def gen_cluster_mean_plot(time, clusterDF, k,cell, ctype, results_dir):
    plt.figure()
    groups = clusterDF.groupby('label')
    fig, ax = plt.subplots()
    for i in range(0, k):
        ax.plot(time, clusterDF.iloc[i,0:4], label = i)
    ax.legend(loc = 'upper right')
    plt.title("Cluster Means over Time " + ctype + " " + cell)
    plt.xlabel("Time in HRs")
    plt.ylabel("Log Base 2 Fold Change")
    ofile = results_dir + "clustermeans_time_"+ctype+"_"+cell+".png"
    plt.savefig(ofile)
    plt.clf()

if __name__ == '__main__':
    plt.rcParams.update({'figure.max_open_warning': 0})
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--idir', help = "input file directory")
    parser.add_argument('--odir', help = "output file directory")

    args = parser.parse_args()

    #generating input file names
    ifile_liver = args.idir + 'liver_processed.csv'
    ifile_intestine = args.idir + 'intestine_processed.csv'
    ifile_kidney = args.idir + 'kidney_processed.csv'
    ifile_muscle = args.idir + 'muscle_processed.csv'

    #reading in the csv files
    liver_tb = np.genfromtxt(ifile_liver, dtype = str, skip_header = 0, delimiter = ",")
    intestine_tb = np.genfromtxt(ifile_intestine, dtype = str, skip_header = 0, delimiter = ",")
    kidney_tb = np.genfromtxt(ifile_kidney, dtype = str, skip_header = 0, delimiter = ",")
    muscle_tb = np.genfromtxt(ifile_muscle, dtype = str, skip_header = 0, delimiter = ",")

    #separating the data from the labels
    liver_data = liver_tb[:,1:5].astype(float)
    intestine_data = intestine_tb[:,1:5].astype(float)
    kidney_data = kidney_tb[:,1:5].astype(float)
    muscle_data = muscle_tb[:,1:5].astype(float)

    #generating tsne features 
    ts = TSNE(n_components = 2)
    liver_tsne = ts.fit_transform(liver_data)
    intestine_tsne = ts.fit_transform(intestine_data)
    kidney_tsne = ts.fit_transform(kidney_data)
    muscle_tsne = ts.fit_transform(muscle_data)
    
    krange = np.arange(2,20)

    #generating bic scores
    liver_bic_km, liver_bic_gmm = bic_plot(liver_data, krange)
    intestine_bic_km, intestine_bic_gmm = bic_plot(intestine_data, krange)
    kidney_bic_km, kidney_bic_gmm = bic_plot(kidney_data, krange)
    muscle_bic_km, muscle_bic_gmm = bic_plot(muscle_data, krange)
    
    #generating bic plots
    gen_bic_plot(krange, liver_bic_km, liver_bic_gmm, "liver", args.odir)
    gen_bic_plot(krange, intestine_bic_km, intestine_bic_gmm, "intestine", args.odir)
    gen_bic_plot(krange, kidney_bic_km, kidney_bic_gmm, "kidney", args.odir)
    gen_bic_plot(krange, muscle_bic_km, muscle_bic_gmm, "muscle", args.odir)
   
    #setting k number of clusters after visual inspection of bic plots
    liver_km_k = 10
    liver_gmm_k = 11
    
    intestine_km_k = 10
    intestine_gmm_k = 10
    
    kidney_km_k = 10
    kidney_gmm_k = 11

    muscle_km_k = 10
    muscle_gmm_k = 13

    #fitting clustering results and creating labels
    liver_km = KMeans(n_clusters = liver_km_k).fit(liver_data)
    liver_gmm = GaussianMixture(n_components = liver_gmm_k).fit(liver_data)
    liver_gmm = liver_gmm.predict(liver_data)
    liver_db = DBSCAN().fit(liver_data)
 
    intestine_km = KMeans(n_clusters = intestine_km_k).fit(intestine_data)
    intestine_gmm = GaussianMixture(n_components = intestine_gmm_k).fit(intestine_data)
    intestine_gmm = intestine_gmm.predict(intestine_data)
    intestine_db = DBSCAN().fit(intestine_data)

    kidney_km = KMeans(n_clusters = kidney_km_k).fit(kidney_data)
    kidney_gmm = GaussianMixture(n_components = kidney_gmm_k).fit(kidney_data)
    kidney_gmm = kidney_gmm.predict(kidney_data)
    kidney_db = DBSCAN().fit(kidney_data)

    muscle_km = KMeans(n_clusters = muscle_km_k).fit(muscle_data)
    muscle_gmm = GaussianMixture(n_components = muscle_gmm_k).fit(muscle_data)
    muscle_gmm = muscle_gmm.predict(muscle_data)
    muscle_db = DBSCAN().fit(muscle_data)
    
    #generating cluster plots
    gen_cluster_plot(liver_tsne, liver_km.labels_, "KMeans","liver",args.odir)
    gen_cluster_plot(liver_tsne, liver_db.labels_, "DBSCAN","liver", args.odir)
    gen_cluster_plot(liver_tsne, liver_gmm, "GMM", "liver", args.odir)
    
    gen_cluster_plot(intestine_tsne, intestine_km.labels_, "KMeans","intestine",args.odir)
    gen_cluster_plot(intestine_tsne, intestine_db.labels_, "DBSCAN","intestine", args.odir)
    gen_cluster_plot(intestine_tsne, intestine_gmm, "GMM", "intestine", args.odir)
    
    gen_cluster_plot(kidney_tsne, kidney_km.labels_, "KMeans","kidney",args.odir)
    gen_cluster_plot(kidney_tsne, kidney_db.labels_, "DBSCAN","kidney", args.odir)
    gen_cluster_plot(kidney_tsne, kidney_gmm, "GMM", "kidney", args.odir)
 
    gen_cluster_plot(muscle_tsne, muscle_km.labels_, "KMeans","muscle",args.odir)
    gen_cluster_plot(muscle_tsne, muscle_db.labels_, "DBSCAN","muscle", args.odir)
    gen_cluster_plot(muscle_tsne, muscle_gmm, "GMM", "muscle", args.odir)
    
    time = np.array([12, 24, 48, 72])
    
    #generating cluster means
    liverDF_km = genDF(liver_km.labels_,liver_data, liver_km_k)
    liverDF_gmm = genDF(liver_gmm, liver_data, liver_gmm_k)
    liverDF_db = genDF(liver_db.labels_, liver_data, 1)
    
    intestineDF_km = genDF(intestine_km.labels_,intestine_data, intestine_km_k)
    intestineDF_gmm = genDF(intestine_gmm, intestine_data, intestine_gmm_k)
    intestineDF_db = genDF(intestine_db.labels_, intestine_data, 1)

    kidneyDF_km = genDF(kidney_km.labels_,kidney_data, kidney_km_k)
    kidneyDF_gmm = genDF(kidney_gmm, kidney_data, kidney_gmm_k)
    kidneyDF_db = genDF(kidney_db.labels_, kidney_data, 1)

    muscleDF_km = genDF(muscle_km.labels_,muscle_data, muscle_km_k)
    muscleDF_gmm = genDF(muscle_gmm, muscle_data, muscle_gmm_k)
    muscleDF_db = genDF(muscle_db.labels_, muscle_data, 1)
    
    #generating cluster mean plots
    gen_cluster_mean_plot(time, liverDF_km, liver_km_k, "liver", "KMeans", args.odir)
    gen_cluster_mean_plot(time, liverDF_gmm, liver_gmm_k, "liver", "GMM", args.odir)
    gen_cluster_mean_plot(time, liverDF_db, 1, "liver", "DBSCAN", args.odir)
 
    gen_cluster_mean_plot(time, intestineDF_km, intestine_km_k, "intestine", "KMeans", args.odir)
    gen_cluster_mean_plot(time, intestineDF_gmm, intestine_gmm_k, "intestine", "GMM", args.odir)
    gen_cluster_mean_plot(time, intestineDF_db, 1, "intestine", "DBSCAN", args.odir)
 
    gen_cluster_mean_plot(time, kidneyDF_km, kidney_km_k, "kidney", "KMeans", args.odir)
    gen_cluster_mean_plot(time, kidneyDF_gmm, kidney_gmm_k, "kidney", "GMM", args.odir)
    gen_cluster_mean_plot(time, kidneyDF_db, 1, "kidney", "DBSCAN", args.odir)
 
    gen_cluster_mean_plot(time, muscleDF_km, muscle_km_k, "muscle", "KMeans", args.odir)
    gen_cluster_mean_plot(time, muscleDF_gmm, muscle_gmm_k, "muscle", "GMM", args.odir)
    gen_cluster_mean_plot(time, muscleDF_db, 1, "muscle", "DBSCAN", args.odir)
    
    #ofile names for .cls files
    liver_cls_gmm = args.odir + 'liver_phenotype_GMM.cls'
    liver_cls_km = args.odir + 'liver_phenotype_KM.cls'
    liver_cls_db = args.odir + 'liver_phenotype_DB.cls'

    intestine_cls_gmm = args.odir + 'intestine_phenotype_GMM.cls'
    intestine_cls_km = args.odir + 'intestine_phenotype_KM.cls'
    intestine_cls_db = args.odir + 'intestine_phenotype_DB.cls'

    kidney_cls_gmm = args.odir + 'kidney_phenotype_GMM.cls'
    kidney_cls_km = args.odir + 'kidney_phenotype_KM.cls'
    kidney_cls_db = args.odir + 'kidney_phenotype_DB.cls'

    muscle_cls_gmm = args.odir + 'muscle_phenotype_GMM.cls'
    muscle_cls_km = args.odir + 'muscle_phenotype_KM.cls'
    muscle_cls_db = args.odir + 'muscle_phenotype_DB.cls'
    
    #writing to CLS files
    writeCLS(liver_cls_gmm, liverDF_gmm.values[:,0:4])
    writeCLS(liver_cls_km, liverDF_km.values[:,0:4])
    writeCLS(liver_cls_db, liverDF_db.values[:,0:4])
    
    writeCLS(intestine_cls_gmm, intestineDF_gmm.values[:,0:4])
    writeCLS(intestine_cls_km, intestineDF_km.values[:,0:4])
    writeCLS(intestine_cls_db, intestineDF_db.values[:,0:4])

    writeCLS(kidney_cls_gmm, kidneyDF_gmm.values[:,0:4])
    writeCLS(kidney_cls_km, kidneyDF_km.values[:,0:4])
    writeCLS(kidney_cls_db, kidneyDF_db.values[:,0:4])

    writeCLS(muscle_cls_gmm, muscleDF_gmm.values[:,0:4])
    writeCLS(muscle_cls_km, muscleDF_km.values[:,0:4])
    writeCLS(muscle_cls_db, muscleDF_db.values[:,0:4])




   
