#Data preprocessing and normalization
import numpy as np
import argparse
import sys
'''
    Standardize each gene to mean 0 and std 1
    @param dat is the data matrix to change
    @return standardized data matrix
'''
def standardize(dat):
    dat = dat.astype(float)
    dat -= np.mean(dat, axis = 1, keepdims = True)
    dat /= np.std(dat, axis = 1, ddof = 1, keepdims = True)
    return dat

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', help = 'data directory')
    args = parser.parse_args()

    #generating input file names
    file_liver = args.dir + 'liver_raw.csv'
    file_intestine = args.dir + 'intestine_raw.csv'
    file_kidney = args.dir + 'kidney_raw.csv'
    file_muscle = args.dir + 'muscle_raw.csv'

    #reading in tables
    table_liver = np.genfromtxt(file_liver, dtype = str, skip_header = 1, delimiter = ',')
    table_intestine = np.genfromtxt(file_intestine, dtype = str, skip_header = 1, delimiter = ",")
    table_kidney = np.genfromtxt(file_kidney, dtype = str, skip_header = 1, delimiter = ",")
    table_muscle = np.genfromtxt(file_muscle, dtype = str, skip_header = 1, delimiter = ",")

    #standardizing
    table_liver[:,1:5] = standardize(table_liver[:,1:5])
    table_intestine[:,1:5] = standardize(table_intestine[:,1:5])
    table_kidney[:,1:5] = standardize(table_kidney[:,1:5])
    table_muscle[:,1:5] = standardize(table_muscle[:,1:5])
    
    #generating output file names
    ofile_liver = args.dir + 'liver_processed.csv'
    ofile_intestine = args.dir + 'intestine_processed.csv'
    ofile_kidney = args.dir + 'kidney_processed.csv'
    ofile_muscle = args.dir + 'muscle_processed.csv'
    
    #writing to outputfiles
    np.savetxt(ofile_liver, table_liver, fmt = '%s', delimiter = ",", newline = "\n")
    np.savetxt(ofile_intestine, table_intestine, fmt = '%s', delimiter = ",", newline = "\n")
    np.savetxt(ofile_kidney, table_kidney, fmt = '%s', delimiter = ",", newline = "\n")
    np.savetxt(ofile_muscle, table_muscle, fmt = '%s', delimiter = ",", newline = "\n")

