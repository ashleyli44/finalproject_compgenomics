#!/bin/bash
data_dir=$1
results_dir=$2
python3 process_data.py --dir $data_dir
python3 test_model.py --idir $data_dir --odir $results_dir
