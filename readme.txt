#Describes data files and expected output files

run the script as 
sh run.sh [data directory] [results directory]

Data Files:
    The data files were read in as csv files containing the gene names and
then the log base 2 fold change for each time point, 12hr vs 0h3, 24hr vs
0hr, 48hr vs 0hr, 72hr vs 0hr. Each cell type was processed in an individual
file and contained a variable amount of genes. 

Output Files:
    For each cell type, 3 different figures were generated. The first figure
was a bic plot for kmeans and gmm. The second figure was a plot of tsne1 vs 
tsne2 colored by cluster labels for each different clustering method. The
third figure was the cluster means over time for each different clustering
method. Additionally, a .cls file was generated for each of the clustering
methods so that the cluster means could be used as phenotypes in the GSEA
analysis 
